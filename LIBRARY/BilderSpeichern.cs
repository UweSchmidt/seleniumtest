using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using System.IO;

public static class BilderSpeichern{
    public static string basestring = "//div[@id='tp-wrapper']/section[@class='main-include-section']//section[@class='main-content-2']/div[@class='inner-wrapper']";            
    public static string endstring = "//div[@class='ts-search-img']";    

    public static void bilderSpeichern() {
        string detail="";
        for(int i=1;i<=anzahlBilderAufSeite();i++) {                    
            detail = "//div[@id='tp-result-" + i.ToString() + "']";
            string image= getImage(getImageURL(basestring + detail + endstring));

            // check if image already exists:
            

            if (image != "-") {
                if (Config.modus == Config.Modus.alle){
                    downloadBild(image, Config.datapath_bilderAlle + Config._aktuellePLZ + "_" + getName(i) + ".jpg");
                }
                else if (Config.modus == Config.Modus.altdorf){
                    if (!File.Exists(Config.bekannte90518 + Config._aktuellePLZ + "_" + getName(i) + ".jpg")){
                        downloadBild(image, Config.neue90518 + Config._aktuellePLZ + "_" + getName(i) + ".jpg");
                    }                 
                }       
            }
        }
        
    }

    public static int anzahlBilderAufSeite() {
        for(int i=1;i<=20;i++) {            
            string detail = "//div[@id='tp-result-" + i.ToString() + "']";
            IWebElement nme;
            try{ // es könnten weniger als 20 Bilder sein
                nme = Globals.driver.FindElement(By.XPath(basestring + detail + "//a"));
            } catch{
                return i-1;
            }
        }
        return 20;
    }

    static string getImage(string imageurl){
          if (Regex.Match(imageurl, ".*frau_thumb.*jpg").Success == false){ // Check auf Dummybild
                string urlpart = Regex.Match(imageurl,"imgUpload.*jp.?g").ToString(); // ein Bild
                if (urlpart == "") {
                    urlpart = Regex.Match(imageurl,"bilder/.*(jp.?g|png)").ToString(); // Bildergallerie
                }
                return "http://ssl.tanzpartner.de/" + urlpart;    	        
            }
            else {return "-";}
    }

    static string getName(int indexBildAufSeite){
        string detail = "//div[@id='tp-result-" + indexBildAufSeite.ToString() + "']";
        IWebElement nme = Globals.driver.FindElement(By.XPath(basestring + detail + "//a")); 

        string name = SE.SE.GetAttributeAsType<string>(nme,"href").Split('_')[1];
        Console.WriteLine(indexBildAufSeite.ToString() + ": " + name);
        return name;
        
    }

    static string getImageURL(string str) {
        IWebElement b = Globals.driver.FindElement(By.XPath(str));
        return SE.SE.GetAttributeAsType<string>(b,"style");
    }


    static void downloadBild(string url, string dateiname) {
        System.Net.WebClient w = new System.Net.WebClient();        
        try{
            w.DownloadFile(url, dateiname);
        } catch {
            Console.WriteLine("Download nicht möglich für: " + Config._aktuellePLZ + "_" + dateiname);
        }
    }
}