public static class Config{
    //public static string datapath_bilderAktuell = @"C:\tmp\tp\aktuell90518\";
    public static string datapath_bilderAlle= @"C:\tmp\tp\alle\";    
    public static string datapath = @"C:\tmp\tp\data\";
    public static string finalePLZListe = datapath + "finalePLZListe.txt";
    public static string gelaufenePLZ = datapath + "gelaufenePLZ.txt";

    public static string _aktuellePLZ = "";


    public static string neue90518= @"C:\tmp\tp\neue90518\";
    public static string bekannte90518= @"C:\tmp\tp\bekannte90518\";


    // Repository
    public static string btn1= "//*[@class='pagination-container']/span[1]";
    public static string btn2= "//*[@class='pagination-container']/a[1]";
    public static string btn3= "//*[@class='pagination-container']/a[3]"; //hat man #2 gewählt, erscheint ein Zurück-Button, daher '3' statt '2'
    public static string btn4= "//*[@class='pagination-container']/a[4]";
    public static string btn5= "//*[@class='pagination-container']/a[5]";
    

    public static Modus modus = Modus.altdorf;

    public enum Modus
    {
      alle, 
      altdorf
    }
    
}

