using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

public static class Globals{

    public static string chromeBinaryLocation = @"C:\Users\Lenovo\Downloads\chrome-win\chrome.exe";
    public static string chromeDriverLocation = @"C:\Users\Lenovo\Documents\Webdriver\Chrome";
    
    public static IWebDriver getChromeDriver() {
        ChromeOptions chromeOptions = new ChromeOptions();    
        chromeOptions.BinaryLocation = chromeBinaryLocation;
        chromeOptions.AddArgument("--window-size=1300,900");

        chromeOptions.DebuggerAddress="127.0.0.1:9222";
        return new ChromeDriver(chromeDriverLocation, chromeOptions);
    }

    
    
    public static IWebDriver driver = getChromeDriver();    
}