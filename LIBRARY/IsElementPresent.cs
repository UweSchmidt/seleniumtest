using OpenQA.Selenium;

public static class IsElementPresent{
    public static bool isElementPresent(By by)
    {
        try{
            Globals.driver.FindElement(by);
            return true;
        }
        catch (NoSuchElementException){return false;}
    }
}