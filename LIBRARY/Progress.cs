using System;
using System.Collections.Generic;
using System.Linq;

public static class Progress {


    public static string leseLetzteGelaufenePLZ(string filename) {
        try{            
            return System.IO.File.ReadLines(filename).Last();
        }catch{ 
            return "-";
        }
    }
        
    public static void schreibeLetzteGelaufenePLZ(string letzteGesuchtePLZ, string filename) {
        using (System.IO.StreamWriter w = new System.IO.StreamWriter(filename, true))
        {
            w.WriteLine(letzteGesuchtePLZ);
        }
    }
    
    public static string naechstePLZ() {
        string letzteGelaufenePLZ = Progress.leseLetzteGelaufenePLZ(Config.gelaufenePLZ);
        List<string> l = System.IO.File.ReadAllLines(Config.finalePLZListe).ToList();
        if (letzteGelaufenePLZ == "-") {
            return l[0];
        }
        
        for(int i = 0; i<l.Count; i++){
            if (l[i].Contains(letzteGelaufenePLZ)) {
                if (i+1== l.Count) { 
                    return l[0];                        
                } else {
                    return l[i+1];
                }                     
            }
        }
        return null;
    }
 }
 
 