using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

public static class Suche{
    public static void suche() {   

        IWebElement txtPLZOrt = Globals.driver.FindElement(By.CssSelector("input#plzort"));
        txtPLZOrt.Clear();
        txtPLZOrt.SendKeys(Config._aktuellePLZ);

        SelectElement cmbWenSucheIch = new SelectElement(Globals.driver.FindElement(By.CssSelector("select[name='kat1suche']")));
        cmbWenSucheIch.SelectByIndex(0);

        SelectElement cmbUmkreis = new SelectElement(Globals.driver.FindElement(By.CssSelector("select#radius")));
        cmbUmkreis.SelectByText("50 km");

        IWebElement chkNurProfileMitFoto = Globals.driver.FindElement(By.XPath("/html//input[@id='bild']"));
        chkNurProfileMitFoto.Click();

        IWebElement btnSucheStarten = Globals.driver.FindElement(By.CssSelector("div#full-width-search button[type='submit']"));
        btnSucheStarten.Click();
    }

    public static void zurueckZurSuche(){
        IWebElement lnkTanzpartnersuche = Globals.driver.FindElement(By.CssSelector(".menu-item > b"));
        lnkTanzpartnersuche.Click();
     }

         

}

