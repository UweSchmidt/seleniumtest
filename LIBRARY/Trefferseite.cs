using System.Collections.Generic;
using OpenQA.Selenium;

public static class InTreffernBlaettern{
    public static void inTreffernBlaettern() {                           
        List<string> fuenfseiten= new List<string>() {Config.btn1, Config.btn2, Config.btn3, Config.btn4,Config.btn5};     

        foreach (string s in fuenfseiten) {
            if(IsElementPresent.isElementPresent(By.XPath(s))) {
                Globals.driver.FindElement(By.XPath(s)).Click();
                BilderSpeichern.bilderSpeichern();
            }
        }
    }


}

