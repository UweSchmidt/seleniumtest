﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;

using System.Drawing;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;


namespace selenium
{
    class Program
    {


        static void Main(string[] args)
        {            
            Config.modus= Config.Modus.altdorf;
            //Login.login();                                                            

            if      (Config.modus == Config.Modus.alle)     {durchsucheAllePLZ();}        
            else if (Config.modus == Config.Modus.altdorf)  {checkeAlleAccountsBeiPLZObNeu("90518");}  //checkeAktuelleAccountsBeiPLZ("90518");              
             
             
             
             // driver.Quit();
        } 
        
        // Alle Trefferseiten bei dieser PLZ
        static void checkeAlleAccountsBeiPLZObNeu(string plz) {
            Config._aktuellePLZ=plz;
            Suche.suche();
            InTreffernBlaettern.inTreffernBlaettern();
        }

        // nur die 1. Seite bei dieser PLZ
        static void checkeAktuelleAccountsBeiPLZ(string plz) { 
            Config._aktuellePLZ=plz;
            Suche.suche();
            
            BilderSpeichern.bilderSpeichern();
        }


        static void durchsucheAllePLZ() {
            while(true){
                Thread.Sleep(5000);
                Config._aktuellePLZ= Progress.naechstePLZ();            
                Suche.suche();            
                InTreffernBlaettern.inTreffernBlaettern();        
                Progress.schreibeLetzteGelaufenePLZ(Config._aktuellePLZ, Config.gelaufenePLZ);
                Suche.zurueckZurSuche();

            }         
        }

    }
}